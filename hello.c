#include <stdio.h>
#include "hello.h"

int main() {
    //printToScreen();
    int sum = addNumbers(4, 5);

    printf("Sum: %i\n", sum);

    int i = 5.556;
    printf("Number: %i", i);
    return 0;
}

int addNumbers(int val1, int val2) {
    return val1 + val2;
}

void printToScreen() {
    printf("The last message to earth\n");
    myStupidFunction();
}

void myStupidFunction() {
    printf("Byebye\n");
}