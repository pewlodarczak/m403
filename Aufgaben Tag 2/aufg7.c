#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    long number;
    int firstDigit, lastDigit, i = 0;
    long origNumber, swapedNumber = 0;

    // Ex. 43752 -> 23754

    printf("Enter a positive integer:\n");
    scanf("%li", &number);
    printf("You entered: %li\n", number);

    origNumber = number;
    lastDigit = number % 10;

    do
    {
        i++;
        number /= 10;
        if(number % 10 != 0)
            firstDigit = number;
    } while(number != 0);
    
    printf("First digit: %i\n", firstDigit);
    printf("Last digit: %i\n", lastDigit);
    printf("Number of digits: %i\n", i);
    return 0;
}