#include <stdio.h>

int main() {
    int iArr[] = {4, 7, 23, 73, 54, 3, 65, 8, 89};
    int biggest = iArr[0], secondbiggest;
    for(int i = 1; i < sizeof(iArr)/sizeof(int); i++) {
        if(iArr[i] > biggest) {
            secondbiggest = biggest;
            biggest = iArr[i];
        }
    }
    printf("second biggest %d\n", secondbiggest);
    printf("biggest %d\n", biggest);
}