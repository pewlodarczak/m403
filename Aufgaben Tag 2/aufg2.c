#include <stdio.h>

int main()
{
    int num = 0, count = 0, biggest = 0;
    printf("Enter number of numbers:\n");
    scanf("%d", &num);
    
    while(count < num)
    {
        int input;
        printf("enter number:\n");
        scanf("%d", &input);
        if(input > biggest)
            biggest = input;
        ++count;
    }
    printf("biggest %d\t", biggest);
    
    return 0;
}
