#include <stdio.h>

void encr(char *msg, int key);
void decr(char *msg, int key);

int main()
{
    char message[100], ch;
    int i, key;
    printf("Enter a message to encrypt: ");
    gets(message);
    printf("Enter key: ");
    scanf("%d", &key);
    encr(message, key);
    printf("Encrypted message: %s\n", message);
    decr(message, key);
    printf("Decrypted message: %s\n", message);
    return 0;
}

void encr(char *msg, int key)
{
    char ch;
    for (int i = 0; msg[i] != '\0'; ++i)
    {
        ch = msg[i];
        if (ch >= 'a' && ch <= 'z')
        {
            ch = ch + key;
            if (ch > 'z')
            {
                ch = ch - 'z' + 'a' - 1;
            }
            msg[i] = ch;
        }
        else if (ch >= 'A' && ch <= 'Z')
        {
            ch = ch + key;
            if (ch > 'Z')
            {
                ch = ch - 'Z' + 'A' - 1;
            }
            msg[i] = ch;
        }
    }
}

void decr(char *msg, int key)
{
    char ch;
    for (int i = 0; msg[i] != '\0'; ++i)
    {
        ch = msg[i];
        if (ch >= 'a' && ch <= 'z')
        {
            ch = ch - key;
            if (ch < 'a')
            {
                ch = ch + 'z' - 'a' + 1;
            }
            msg[i] = ch;
        }
        else if (ch >= 'A' && ch <= 'Z')
        {
            ch = ch - key;
            if (ch < 'A')
            {
                ch = ch + 'Z' - 'A' + 1;
            }
            msg[i] = ch;
        }
    }
}
