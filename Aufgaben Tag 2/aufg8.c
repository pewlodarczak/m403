#include <stdio.h>
#include <stdbool.h>
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

int main() {
    int count = 0;    
    bool running = true;
    printf("\n\t");
    while (running)
    {
        printf("#");
        Sleep(1000);
        count++;
        if(count > 5)
            running = false;
    }
    printf("\n\n");

}