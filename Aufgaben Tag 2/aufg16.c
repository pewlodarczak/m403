#include <stdio.h>
#include <stdbool.h>
#include <string.h>

int main() {
    //char str[] = "Die Liebe ist Sieger; stets rege ist sie bei Leid";
    char str[] = "Trug Tim eine so helle Hose nie mit Gurt?";
    int len = strlen(str);
    bool isPali = true;
    strupr(str); // in Grossbuchstaben umwandeln
    //printf("%s\n", str);
    char str2[len]; // str2 ohne Sonderzeichen
    int i2 = 0;
    for(int i = 0; i < len; i++) {
        //printf("%c", str[i]);
        // Nur Buchstaben, keine Leerschläge oder Sonderzeichen in das neue Array kopieren 
        if(str[i] >= 'A' && str[i] <= 'Z') {
            str2[i2] = str[i];
            ++i2;
        }
    }
    str2[i2] = '\0';
    //printf("\n%s\n", str2);
    len = strlen(str2);
    int end = len-1;
    // Check ob es ein Palindrom ist
    for (int i = 0; i < len / 2; i++)
    {
        if (str2[i] != str2[end])
        {
            isPali = false;
            break;
        }
        --end;
    }
    printf("%s ist ein Palindrom %s\n", str, isPali ? "true" : "false");
}