#include <stdio.h>
#include <stdbool.h>

int main()
{
    int i = 0;
    bool isPrime = true;
    unsigned long long num;

    printf("Enter a number: ");
    scanf("%lld", &num);

    if (num == 0 || num == 1)
        isPrime = true;

    for (i = 2; i <= num / 2; ++i)
    {
        // if n is divisible by i, then n is not prime
        // change flag to 1 for non-prime number
        if (num % i == 0)
        {
            isPrime = false;
            break;
        }
    }

    // flag is 0 for prime numbers
    if (isPrime)
        printf("%d is a prime number.", num);
    else
        printf("%d is not a prime number.", num);

    return 0;
}