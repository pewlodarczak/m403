#include <stdio.h>

int main() {
    int intArr[4][3] = {{51, 43, 45},
                        {51, 43, 45},
                        {81, 23, 45},
                        {57, 43, 45}};
    for(int r = 0; r < 4; r++) {
        for(int c = 0; c < 3; c++) {
            printf("%d ", intArr[r][c]);
        }
        printf("\n");
    }
}