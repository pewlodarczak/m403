#include <stdio.h>

void sort(int *intArray);

int main() {
    int intA[3] = {10, 100, 12};
    for(int i = 0; i < sizeof(intA)/sizeof(int); i++) {
        printf("%d ", intA[i]);
    }
    printf("\n");
    sort(intA);
    for(int i = 0; i < sizeof(intA)/sizeof(int); i++) {
        printf("%d ", intA[i]);
    }
}

void sort(int *intArray) {
    for(int i = 0; i < sizeof(intArray)/sizeof(int); i++) {
            if(intArray[i] > intArray[i+1]) {
                int tmp = intArray[i+1];
                intArray[i+1] = intArray[i];
                intArray[i] = tmp;
            }
    }
    if(intArray[0] > intArray[1]) {
        int tmp = intArray[0];
        intArray[0] = intArray[1];
        intArray[1] = tmp;
    }
}