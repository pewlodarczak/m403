#include <stdio.h>
#include <stdbool.h>

int main() {
    bool running = true;
    unsigned int level;

    do {
        printf("Select your level:\n");
        printf("1 Beginner\n");
        printf("2 Intermediate\n");
        printf("3 Advanced\n");
        printf("4 Exit\n");
        scanf("%d", &level);

        switch(level) 
        {
            case 1:
                printf("You are Beginner\n");
                break;
            case 2:
                printf("You are Intermediate\n");
                break;
            case 3:
                printf("You are Advanced\n");
                break;
            case 4:
                printf("Byebye\n");
                running = false;
                break;
            default:
                printf("Wrong input\n");
                break;
        }
    } while(running);

}
