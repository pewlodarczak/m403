#include <stdio.h>
#include <stdbool.h>
#include <string.h>

bool isPalindrome(char *str);

int main() {
    char *str = "sugus";
    //char *str = "Die Liebe ist Sieger; stets rege ist sie bei Leid";
    if(isPalindrome(str)) {
        printf("%s is palindrome\n", str);
    } else
        printf("%s is no palindrome\n", str);
    
}

bool isPalindrome(char *str) {
    bool isPali = true;
    int len = strlen(str);
    len -= 1;
    for(int i = 0; i < strlen(str); i++) {
        if(str[i] != str[len]) {
            isPali = false;
            return isPali;
        }
        len--;
    }
    return isPali;
}
