#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int counter = 0;

int *odd(int *arr, int size)
{
    int *tmp = (int *)malloc(sizeof(arr) * sizeof(int));
    for (size_t i = 0; i < size; i++)
    {
        if (arr[i] % 2 == 1)
        {
            tmp = (int *)realloc(tmp, (counter + 1) * sizeof(int));
            tmp[counter++] = arr[i];
        }
        for (size_t i = 0; i < counter; i++)
        {
            printf("%d\n", arr[i]);
        }
    }
    return tmp;
}

int main()
{
    int arr[] = {3, 6, 2, 8, 5, 9, 1, 11, 2, 5};
    // int odd[10];
    // int even[10];
    int size = sizeof(arr) / sizeof(int);
    printf("Even: ");
    int *tmp = odd(arr, size);
    printf("\nOdd: ");
    for (size_t i = 0; i < counter; i++)
    {
        printf("%d\n", tmp[i]);
    }
    return 0;
}