#include <stdio.h>
#include <stdlib.h>

//int intArr[] = {3, 6, 2, 8, 5, 9, 1, 11};

int main()
{
    int elem, count = 0, size = 8;
    int *ptr = (int*)malloc(size*sizeof(int));
    int arr[] = {3, 6, 2, 8, 5, 9, 1, 11};
    for(int i = 0; i < size; i++) {
        ptr[i] = arr[i];
    }
    printf("Which odd element do you want?\n");
    scanf("%d", &elem);
    for(int i = 0; i < sizeof(ptr); i ++) {
        if(ptr[i]%2 !=0) {
            count++;
        }
        if(count == elem) {
            printf("%d odd elem is %d index %d\n", elem, ptr[i], i);
            return 0;
        }
    }
    printf("Sorry, only %d odd elements\n", count);
    free(ptr);
    return 0;
}
