#include <stdio.h>

int factorial(int n);

int main()
{
    int n = 0;
    printf("Factorial of which integer?\n");
    scanf("%d", &n);
    printf("Factorial of %d: %d\n", n, factorial(n));
}

int factorial(int n)
{
    if (n == 0)
    {
        return 1;
    }
    else
    {
        return n * factorial(n - 1);
    }
}
