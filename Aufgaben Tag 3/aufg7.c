#include <stdio.h>

int main()
{
    FILE *fptr;
    int c;
    fptr = fopen("emp.txt", "r");

    if (fptr == NULL)
    {
        printf("File does not exist.\n");
        return 0;
    }

    if (fptr)
    {
        while ((c = getc(fptr)) != EOF)
            putchar(c);
        fclose(fptr);
    }
    return 0;
}