#include <stdio.h>

int intArr[3][8] = {{3, 6, 2, 8, 5, 9, 1, 11}, 
                  {2, 7, 0, 2, 4, 8, 2, 13}, 
                  {2, 1, 10, 2, 4, 3, 2, 1}};

int main()
{
    for(int r = 0; r < 3; r++) {
        for(int c = 0; c < 8; c++) {
            if(intArr[r][c] == 3)
                printf("intArr[%d][%d] contains a 3\n", r, c);
        }
    }
}


