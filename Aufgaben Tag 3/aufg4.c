#include <stdio.h>

int intArr[] = {3, 6, 2, 8, 5, 9, 1, 11};

int main()
{
    int elem, count = 0;
    printf("Which odd element do you want?\n");
    scanf("%d", &elem);
    for(int i = 0; i < sizeof(intArr)/sizeof(int); i ++) {
        if(*(intArr+i)%2 !=0) {
            count++;
        }
        if(count == elem) {
            printf("%d odd elem is %d index %d\n", elem, *(intArr+i), i);
            return 0;
        }
    }
    printf("Sorry, only %d odd elements\n", count);
    return 0;
}
