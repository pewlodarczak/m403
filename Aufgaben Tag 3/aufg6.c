#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    time_t t;
    srand((unsigned) time(&t));
	int r = 3, c = 8;
	int* ptr = malloc((r * c) * sizeof(int));
	for (int i = 0; i < r * c; i++)
		ptr[i] = rand() % 10;

    // Pretend being a 2D array
	for (int i = 0; i < r; i++) {
		for (int j = 0; j < c; j++) {
            //printf("intArr[%d][%d] = %d\n", i, j, ptr[i * c + j]);
                if(ptr[i * c + j] == 3) {
                    printf("intArr[%d][%d] contains a 3\n", i, j);
                }
        }
	}
	free(ptr);
	return 0;
}
