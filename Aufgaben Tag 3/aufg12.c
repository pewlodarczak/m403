#include <stdio.h>
#include <conio.h>
#include <string.h>

int main()
{
    char str[100], cTmp;
    int i, j, len;
    printf("Enter any string: ");
    gets(str);
    len = strlen(str);
    for (i = 0; i < len; i++)
    {
        for (j = 0; j < (len - 1); j++)
        {
            if (str[j] > str[j + 1])
            {
                cTmp = str[j];
                str[j] = str[j + 1];
                str[j + 1] = cTmp;
            }
            printf("%s\n", str);
        }
    }
    printf("\nSame string in ascending order:\n%s\n", str);
    // check if isogran
    for (i = 0; i < len - 1; i++)
    {
        if (str[i] == str[i + 1])
        {
            printf("No isogram");
            return 0;
        }
    }
    printf("%s is an isogram\n", str);
    return 0;
}