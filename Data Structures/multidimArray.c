#include <stdio.h>

void multiArr();

int main() {
    
  int values[5][2] = {
      {0, 1},
      {5, 6},
      {7, 3},
      {4, 9},
      {1, 6}
  };

    printf("\nvalues[5][2]: \n");
    
  for(int i = 0; i < 5; ++i) {
      for(int j = 0; j < 2; j++) {
          printf("%d ", values[i][j]);
      }
      printf("\n");
  }
  return 0;
}

void multiArr() {
    float mark[2][5] = {{5, 4.5, 5.5, 4.5, 6}, {5, 4.5, 5.5, 4.5, 6}};
    //float mark[2][5] = {5, 4.5, 5.5, 4.5, 6, 5, 4.5, 5.5, 4.5, 6};
    printf("\n");
    for(int i = 0; i < 2; i++) {
        for (int j = 0; j < 5; j++) {
            printf("mark[%d][%d] = %.1f \n", i, j, mark[i][j]);
        }
        //printf("\n");
    }
}