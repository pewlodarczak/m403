#include <stdio.h>

void changeArray(char *anArr);

int main() {
    char dArray[] = {'a', 'b', 'c'};
    changeArray(dArray);
    for(int i = 0; i < 3; i++) {
        printf("%c", dArray[i]);
    }
}

void changeArray(char *anArr) {
    anArr[2] = 'd';
}