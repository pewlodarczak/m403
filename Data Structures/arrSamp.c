#include <stdio.h>

int main() {
    const int row = 3, col = 2;
    double aArr[row][col] ={{4.567, 3.2346}, {45.87654, 32.98765}, {23.63666, 146.654}};
    //printf("%.2lf %.2lf\n", aArr[0], aArr[1]);
    printf("%d\n", sizeof(aArr)/sizeof(double));

    for(int r = 0; r < row; r++) {
        for(int c = 0; c < col; c++) {
            printf("%.4lf\n", aArr[r][c]);
        }
    }
}