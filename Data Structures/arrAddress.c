#include <stdio.h>

int main() {
    int arr[5] = {4, 7, 3, 2, 8};
    
    printf("Address arr: %p\n", arr);
    /* is equivalent to: */
    printf("Address arr: %p\n", &arr[0]);
    printf("Address arr[1]: %p\n", arr+1);
    /* is equivalent to: */
    printf("Address arr[1]: %p\n", &arr[1]);
    printf("Value arr[3]: %d\n", *(arr+3));
    /* is equivalent to: */
    printf("Value arr[3]: %d\n", arr[3]);
}

