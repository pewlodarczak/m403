#include <stdio.h>
#include <stdlib.h>

int main()
{
	char row0[] = "argument";
	char row1[] = "command";
	char* jagged[2] = { row0, row1 };

	int Size[2] = { 8, 7 }, k = 0;
	for (int i = 0; i < 2; i++) {
		char* ptr = jagged[i];

		for (int j = 0; j < Size[k]; j++) {
			printf("%c ", *ptr);
			ptr++;
		}

		printf("\n");
		k++;

		// move the pointer to the next row
		jagged[i]++;
	}
	return 0;
}
