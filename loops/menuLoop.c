#include <stdio.h>

void showMenu() {
    int selection;
    int count = 0;

    for( ; ;) {
        printf("Select your level: \n");
        printf("1.\t Beginner\n");
        printf("2.\t Intermediate\n");
        printf("3.\t Advanced\n");
        printf("4.\t Exit\n");
        printf("\nEnter your level: ");
        scanf("%d", &selection);
        //printf("Selection %c\n", selection);

        switch(selection) {
            case 1:
                printf("\nBeginner\n");
                break;
            case 2:
                printf("\nIntermediate\n");
                break;
            case 3:
                printf("\nAdvanced\n");
                break;
            case 4:
                printf("\nByebye\n");
                return 0;
                break;
            default:
                printf("Enter a correct level!\n");
                break;
        }
    }
}

int main()
{
    showMenu();
    return 0;
}