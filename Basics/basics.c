#include <stdio.h>
#include <windows.h>
#include <time.h>

void printToScreen(const char* message);
void printToScreenNew(char* message);
void printHashes();
void getSize();
int* getRand();

int main()
{

    printf("Lets start\n");
    printf("Get rand array:\n");
    int* rnd = getRand();

    for (int i = 0; i < 10; i++) {
        printf("rand num %d: %d\n", i, rnd[i]);
    }

    int val = 9;
    int *ip;
    ip = &val;

    // Help text
    const char message[] = "The last message to earth!\n";
    printf("sizeof message %d\n", sizeof(message));
    printf("strlen message %d\n", strlen(message));

    getSize();
    /*
      Help text two
    */
    //printToScreen(message);
    
    char newMessage[] = "All the new things!\n";
    printToScreenNew(newMessage);
    printHashes();

    return 0;
}


void getSize()
{

    int var = 10;
    printf("Size of var: %d bites\n", sizeof(var));

    char name[] = "Gobbledygook";
    // Dangerous, use strlen() instead
    printf("Size of name: %d bites\n", sizeof(name));
    printf("Name: %s\n", name);

}
void printToScreen(const char *message)
{
    printf(message);
}

void printToScreenNew(char* message)
{
    printf(message);
}

void printHashes()
{
    int running = 1;
    while (running)
    {
        printf("\n\t#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#\n");
        /*
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#");
        Sleep(1000);
        printf("#\n");
*/
        running = 0;
    }
}

int* getRand()
{
    static int rnd[10];

    /* set the seed */
    srand((unsigned)time(NULL));

    for (int i = 0; i < 10; i++) {
        rnd[i] = rand();
    }

    return rnd;
}