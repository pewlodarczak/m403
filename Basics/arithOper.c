#include <stdio.h>

int main() {
    int score = 5; // Assignment
    printf("Score %i\n", score);
    score += 1;
    printf("New score %i\n", score);
    score++;
    printf("New score %i\n", score);
    printf("New score %i\n", score++);
    printf("New score %i\n", score);
    return 0;
}