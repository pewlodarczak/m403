#include <stdio.h>

int main()
{
    int var = 10;
    printf("Size of var: %d bites\n", sizeof(var));

    char name[] = "Gobbledygook";
    // Dangerous, use strlen() instead
    printf("Size of name: %d bites\n", sizeof(name));
    printf("Name: %s\n", name);

    return 0;
}
