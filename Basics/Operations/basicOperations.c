#include "basicOperations.h"
#include <stdio.h>

double multi(double val1, double val2)
{
    return val1 * val2;
}

double sum(double val1, double val2)
{
    return val1 + val2;
}
