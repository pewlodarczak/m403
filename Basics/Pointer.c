#include <stdio.h>

void incr(int val);
void incre(int *val);

int main() {

	int  var = 20;   /* actual variable declaration */
	int *pVar = NULL;  /* pointer variable declaration */
	pVar = &var;       /* store address of var in pointer variable*/

	incr(var);
	printf("%d\n", var);

	incre(&var);
	printf("%d\n", var);

/*
	printf("Address of var variable: %p\n", &var);

	/* address stored in pointer variable */
	//printf("Address stored in pVar variable: %p\n", pVar);

	/* access the value using the pointer */
	//printf("Value of *pVar variable: %d\n", *pVar);

	return 0;
}

void incr(int val) {
	++val;
	printf("%d\n", val);
}

void incre(int *val) {
	++*val;
}
