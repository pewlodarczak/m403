#include <stdio.h>
#include <stdlib.h>

int* getRand();

int main()
{
    printf("Lets start\n");
    int* rnd = getRand();

    for (int i = 0; i < 10; i++) {
        printf("rand num %d: %d\n", i, rnd[i]);
    }

    return 0;
}

int* getRand()
{
    static int rnd[10];

    for (int i = 0; i < 10; i++) {
        rnd[i] = rand();
    }

    return rnd;
}