#include <stdio.h>
#include <string.h>

struct Books
{
    char title[50];
    char author[50];
    char subject[50];
    double 	book_id;
} book;


int main() {

    strcpy(book.title, "1984");
    strcpy(book.author, "George Orwell");
    strcpy(book.subject, "Fiction");
    book.book_id = 123456789;

    printf("Title: %s\n", book.title);
    printf("Author: %d\n", book.author);
    printf("ID: %.0f", book.book_id);
    return 0;
}