#include <stdio.h>

void passByValue(int val);

int main()
{
    int val = 10;			// original value
    printf("Value: %d\n", val);
    passByValue(val);
    printf("Value: %d\n", val); // original value unchanged
    return 0;
}

void passByValue(int val)
{
    printf("Value: %d\n", val);
    val++;   				// increment local copy
    printf("Value incremented: %d\n", val);
}
