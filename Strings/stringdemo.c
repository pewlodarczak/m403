#include <stdio.h>
#include <string.h>

int main() {

	char str1[] = "Luke";
	char str2[] = "Skywalker";
	char str3[12];
	int  len;

	/* copy str1 into str3 */
	strcpy(str3, str1);
	printf("strcpy( str3, str1) :  %s\n", str3);

	/* concatenates str1 and str2 */
	strcat(str1, str2);
	printf("strcat( str1, str2):   %s\n", str1);

	/* compare  str1 and str2 */
	/* Returns 0 if s1 and s2 are the same; less than 0 if s1<s2; greater than 0 if s1>s2 */
	printf("strcmp( str1, str2):   %i\n", strcmp(str1, str2));

	/* total lenghth of str1 after concatenation */
	len = strlen(str1);
	printf("strlen(str1) :  %d\n", len);

	return 0;
}
