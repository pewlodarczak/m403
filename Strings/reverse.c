#include <stdio.h>
#include <string.h>

int main() {
    char *str = "riproaring";
    int end = strlen(str);
    char inv[end];
    int beg = 0;
    for(int i = end-1; i >= 0; i--) {
        *(inv+beg) = str[i];
        ++beg;
    }
    inv[end] = '\0';
    printf("%s", inv);
}