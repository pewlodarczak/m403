#include <stdio.h>
#include <string.h>

int main() {
    register int aVal = 10;
    char aStr[] = "Gobbledygook";
    printf("%s\n", aStr);
    printf("%d\n", strlen(aStr));
    for(int i = 0; i < strlen(aStr); i++) {
        printf("%c", aStr[i]);
    }

    char anotherString[50];
    strcpy(anotherString, "Donald Duck");
    printf("%s\n", anotherString);
    printf("%d\n", strlen(anotherString));
}
