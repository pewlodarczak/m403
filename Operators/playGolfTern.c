#include <stdio.h>
#include <stdbool.h>

bool play(float temper, float humidity);

int main() {
    printf("Play %d \n", play(35, 60));
}

bool play(float temper, float humidity) {
    float temp = 30, humid = 60;
    return temper > temp && humidity > humid ? false : true;
}