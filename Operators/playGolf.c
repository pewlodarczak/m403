#include <stdio.h>

int main()
{
    float temp, humid;
    printf("Enter the temperature\n");
    scanf("%f", &temp);
    
    printf("Enter the humidity\n");
    scanf("%f", &humid);
    
    if(temp > 30 && humid > 60) {
        printf("Chances of thunderstorm\n");
    }
    else
        printf("Safe to play Golf\n");

    return 0;
}
