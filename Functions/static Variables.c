#include <stdio.h>

int max(int num1, int num2);

int main()
{
    int val1 = 5, val2 =5;
    printf("Number 1: %i \t number 2: %i\n", val1, val2);
    printf("Bigger number is: %i\n", max(val1, val2));
    val1 = 4, val2 = 6;
    printf("Bigger number is: %i\n", max(val1, val2));
    return 0;
}

int max(int num1, int num2) {
    static int counter = 0;
    printf("Conuter: %i\n", counter);
    counter++;
   /* local variable declaration */
   int result;
   if (num1 > num2)
      result = num1;
   else
      result = num2;
 
   return result; 
}