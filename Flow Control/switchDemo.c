#include <stdio.h>

int main() {
    int num;

    printf("Enter anumber (1 - 5): ");
    scanf("%d", &num);

    switch(num)
    {
        case 1:
            printf("%d",num);
            break;
        case 2:
            printf("%d",num);
            break;
        default:
            printf("Wrong number");
    }

    return 0;
}
