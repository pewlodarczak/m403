#include "pruef1.h"

int main(int argc, char **argv)
{
    int i;
    if (argc == 1 || argc > 2)
    {
        printf("Which exercise do you want to see?\n");
        printf("Args %d\n", argc);
        printf("Enter: pruef1 1 || 2 || 3 || 4\n");
        return -1;
    }
    if (argv[1][0] == '1')
    {
        exer1();
    }
    else if (argv[1][0] == '2')
    {
        exer2();
    }
    else if (argv[1][0] == '3')
    {
        exer3();
    }
    else if (argv[1][0] == '4')
    {
        exer4();
    }
    printf("\n");
    return 0;
}

void exer1()
{
    float age = 0;
    printf("Enter your age:\n");
    scanf("%f", &age);
    if (age < 0)
    {
        printf("Age not valid\n");
        return;
    } else if (age < 16)
    {
        printf("No alc\n");
    } else if (age >= 16 && age < 18)
    {
        printf("Beer or wine\n");
    }
        else
        printf("Drink as much as you want\n");
}

void exer2()
{
    float hight = 0;
    char conv;
    printf("Enter your hight:\n");
    scanf("%f", &hight);
    printf("Inch: i or Feet: f\n");
    scanf(" %c", &conv);
    if (conv == 'i')
        printf("Inch %.2f\n", meterToInch(hight));
    else if (conv == 'f')
        printf("Feet %.2f\n", meterToFeet(hight));
    else
        printf("Input invalid!\n");
}

float meterToInch(float hight) {
    return (hight / 0.0254);
}

float meterToFeet(float hight) {
    return (hight * 3.280839895);
}

void exer3() {
    int val1 = 0, val2 = 0, val3 = 0, rand = genRand();
    printf("Enter three integer:\n");
    scanf("%d %d %d", &val1, &val2, &val3);
    if (val1 == rand || val2 == rand || val3 == rand)
        printf("Congrats %d\n", rand);
    else
        printf("Better luck next time\n");
}

int genRand() {
    time_t t;
    srand((unsigned) time(&t));
    return rand() %100;
}

void exer4() {
    int year = 0;
    printf("Enter year since 2000:\n");
    scanf("%d", &year);
    if (!leapOrNot(year))
        printf("%d is leap year\n", year);
    else
        printf("%d no leap year\n", year);
}

bool leapOrNot(int year) {
    return year%4;
}