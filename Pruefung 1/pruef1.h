#ifndef PRUEF1_H
#define PRUEF1_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

void exer1();
void exer2();
void exer3();
void exer4();
float meterToInch(float hight);
float meterToFeet(float hight);
int genRand();
bool leapOrNot(int year);

#endif