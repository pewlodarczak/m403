#include "pruef3.h"

int main() {
    exer1();
}

void exer1(void) {
    char cArr[] = {'a', 'b', 'f', 'r', 's'};
    int add = 3;
    extendArr1(cArr, add);
    printf("1 new arr %s new arr size %d\n", cArr, strlen(cArr));
    
    char cArr2[] = {'a', 'b', 'f', 'r', 's', '\0'};
    char *word = NULL;
    //word = (char*)malloc(strlen(cArr2) * sizeof(char));
    word = (char*)malloc(strlen(cArr2) + 1);
    strcpy(word, cArr2);
    extendArr2(word, 3);
    printf("2 new arr %s new arr size %d\n", word, strlen(word));   
}

void extendArr1(char arr[], int toAdd) {
    int size = strlen(arr), newSize = strlen(arr) + toAdd;
    char stoAdd[toAdd];
    for(int i = 0; i < toAdd; i++) {
        stoAdd[i] = ' ';
    }
    stoAdd[toAdd] = '\0';
    strcat(arr, stoAdd);
}

void extendArr2(char arr[], int toAdd) {
    arr = realloc(arr, sizeof(char)*strlen(arr) + 1);
    char stoAdd[toAdd];
    for(int i = 0; i < toAdd; i++) {
        *(stoAdd + i) = ' ';
    }
    stoAdd[toAdd] = '\0';
    strcat(arr, stoAdd);
}