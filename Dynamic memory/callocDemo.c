#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const int maxelem = 10;
float randomFloat();

int main()
{
    int i;
    float *element;
    element=(float*)calloc(maxelem,sizeof(float)); 
    if(element==NULL)
    {
        printf(" No memory is allocated.");
        exit(0);
    }
    printf("\n");
    srand((unsigned int)time(NULL));
    for(i = 0; i < maxelem; ++i)  
    {
       *(element + i) = randomFloat();
       printf("%f\n", *(element + i));
    }
    for(i = 1; i < maxelem + 1; ++i)  
    {
       if(*element<*(element+i)) 
           *element=*(element+i);
    }
    printf(" The Largest element is :  %.2f \n\n",*element);
    return 0;
}

float randomFloat() {
    return ((float)rand()/(float)(RAND_MAX)) * 10;
}
