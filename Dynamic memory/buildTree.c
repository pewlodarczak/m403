#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "buildTree.h"

int main()
{
    int height;
    printf("Enter height:\n");
    scanf("%d", &height);
    buildTree(height);
    return 0;
}

void buildTree(int height)
{
    printf("\n\n");
    if (height < 4)
    {
        printf("Height has to be at least 4");
        return;
    }

    int i = 0, spaces = height - 2, tmpSpc = spaces;
    int maxWidth = height + (height - 3);
    char *starLine = NULL, *line = NULL;
    starLine = (char*)malloc(maxWidth * sizeof(char));
    if(starLine == NULL) {
        printf("Error! memory not allocated.");
        exit(0);
    }
    line = (char*)malloc(maxWidth * sizeof(char));
    if(line == NULL) {
        printf("Error! memory not allocated.");
        exit(0);
    }
    starLine[0] = '\0';
    line[0] = '\0';
    while (i < (height - 1))
    {
        while (spaces > 0)
        {
            strcat(line, " ");
            spaces -= 1;
        }
        tmpSpc -= 1;
        spaces = tmpSpc;
        strcat(line, "*");
        strcat(line, starLine);
        strcat(starLine, "**");
        printf("%s\n", line);
        i += 1;
        line[0] = '\0';
    }
    spaces = height - 2;
    while (spaces > 0)
    {
        strcat(line, " ");
        spaces -= 1;
    }
    strcat(line, "*");
    printf("%s\n", line);
}