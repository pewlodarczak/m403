#include <stdio.h>
#include <stdlib.h>

#define MAX_SENTENCE 5

int main() {
    
    char *word = NULL;
    word = (char*)malloc(MAX_SENTENCE * sizeof(char));
    printf("Enter word:\n");
    scanf("%[^\n]%*c", word);
    printf("Word: %s\n", word);
    
    word = (char*)realloc(word, 40 * sizeof(char));
    printf("Enter long word:\n");
    scanf("%[^\n]%*c", word);
    printf("Long word: %s\n", word);
    free(word);
    return 0;
}