#ifndef PRUEF3_H
#define PRUEF3_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

void exer1(void);
void extendArr1(char arr[], int toAdd);
void exer2(void);
void extendArr2(char arr[], int toAdd);

#endif