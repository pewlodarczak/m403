#include <stdio.h>
#include <stdlib.h>

struct student {
	char *name;
    char subject[80];
	int age;
};

struct student* std = NULL;

int main()
{
	std = (struct student*)malloc(sizeof(struct student));
    std->name = "John Do";
	std->age = 18;

    printf("%s\n", std->name);
	printf("%d\n", std->age);

	return 0;
}
