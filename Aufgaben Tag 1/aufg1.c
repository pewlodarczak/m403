#include <stdio.h>
#include "aufg1.h"

int val = 8;

int main() {
    printf("Aufg 1: %f\n", aufg1(3, 7, 9));
    printf("Aufg 2: %f\n", aufg2(135));
    printf("Aufg 5: %f\n", aufg5(3, 7, 9));
    scopeDemo();
}

float aufg1(float val1, float val2, float val3) {
    return val1 * val2 * val3;
}

float aufg2(int min) {
    return min * sec;
}

float aufg5(float val1, float val2, float val3) {
    printf("val %d\n", val);
    return (val1 + val2 + val3)/3;
}

void scopeDemo() {
    printf("val %d\n", val);
}