#include <stdio.h>

void evenOrOdd(int num);
void playGolf(float temp, float humi);
void vowelOrNot(char aChar);

int main() {
    int num;
    printf("Enter a number");
    scanf("%d", &num);
    evenOrOdd(num);
    playGolf(20, 70);
    vowelOrNot('e');
}

void evenOrOdd(int num) {
    if (num%2 == 0) {
        printf("%d is even\n", num);
    } else {
        printf("%d is odd\n", num);
    }
}

void playGolf(float temp, float humi) {
    if (temp > 30 && humi) {
        printf("Don't play\n");
    } else {
        printf("Play\n");
    }
}

void vowelOrNot(char aChar) {
    if(aChar == 'a' || aChar == 'e') {
        printf("Is vowel");
    } else{
        printf("No vowel");
    }
}
