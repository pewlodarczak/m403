#ifndef PRUEF1_H
#define PRUEF1_H

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

void exer1(void);
double convToEur(double curr);
double convToDol(double curr);
void exer2(void);
void printArr(int iArr[], int i, int len);
void exer3(void);
void countVowelCons(char str[]);
void exer4(void);
void reverse(char str[], int i, int len);
void exer5(void);
void exer6(void);
void calcAver(int *arr, int len, float *average);
void exer7(void);


#endif