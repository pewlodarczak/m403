#include "pruef2.h"

int main(int argc, char **argv)
{
    int i;
    if (argc == 1 || argc > 2)
    {
        printf("Which exercise do you want to see?\n");
        printf("Enter: pruef2 1 || 2 || 3 || 4 || 5 || 6 || 7\n");
        return -1;
    }
    if (argv[1][0] == '1')
    {
        exer1();
    }
    else if (argv[1][0] == '2')
    {
        exer2();
    }
    else if (argv[1][0] == '3')
    {
        exer3();
    }
    else if (argv[1][0] == '4')
    {
        exer4();
    }
    else if (argv[1][0] == '5')
    {
        exer5();
    }
    else if (argv[1][0] == '6')
    {
        exer6();
    }
    else if (argv[1][0] == '7')
    {
        exer7();
    }
    printf("\n");
    return 0;
}

void exer1(void)
{
    double amount = 0;
    char c;
    bool running = true;
    do
    {
        printf("Enter an amount to convert:\n");
        scanf("%lf", &amount);
        printf("Convert money to:\n");
        printf("E\tEuro\n");
        printf("D\tDollar\n");
        printf("Q\tQuit\n");
        scanf(" %c", &c);
        if (c == 'E')
        {
            printf("%.2lf in Euro: %.2lf\n", amount, convToEur(amount));
        }
        else if (c == 'D')
        {
            printf("%.2lf in Dollar: %.2lf\n", amount, convToDol(amount));
        }
        else if (c == 'Q')
        {
            printf("Byebye\n");
            running = false;
        }
        else
        {
            printf("Wrong input");
        }
    } while (running);
}

double convToEur(double curr)
{
    return curr * 0.92;
}

double convToDol(double curr)
{
    return curr * 1.09;
}

void exer2(void)
{
    int intArr[] = {3, 6, 2, 8, 5, 9, 1, 7};
    printArr(intArr, 0, sizeof(intArr) / sizeof(int));
}

void printArr(int iArr[], int i, int len)
{
    if (i >= len)
    {
        return;
    }
    printf("%d", iArr[i]);
    printArr(iArr, i + 1, len);
}

void exer3(void)
{
    char str[] = "riproaring thigh slapping";
    countVowelCons(str);
}

void countVowelCons(char str[])
{
    printf("Exer 3\n");
    int vow = 0, con = 0, blank = 0, len = strlen(str), i;
    printf("Str %s len %d\n", str, len);
    for (i = 0; i < len; i++)
    {
        if (str[i] == 'a' || str[i] == 'e' || str[i] == 'i' ||
            str[i] == 'o' || str[i] == 'u' ||
            str[i] == 'A' || str[i] == 'E' || str[i] == 'I' ||
            str[i] == 'O' || str[i] == 'U')
        {
            ++vow;
        }
        else if (str[i] == ' ')
        {
            ++blank;
        }
        else if ((str[i] >= 'a' && str[i] <= 'z'))
        {
            ++con;
        }
    }

    printf("%s contains %d vowels, %d consonants, %d blanks\n", str, vow, con, blank);
}

void exer4(void)
{
    char str[] = "margorp emosewa";
    int len = strlen(str);
    reverse(str, 0, len - 1);
    printf("Str %s\n", str);
}

void reverse(char str[], int i, int len)
{
    if (i >= len)
        return;
    char c = str[i];
    str[i] = str[len];
    str[len] = c;
    reverse(str, i + 1, len - 1);
}

void exer5(void)
{
    char str[] = "Hey diddle, diddle! The cat and the fiddle";
    int len = strlen(str), frequency = 0;
    char charToFind = 'd';
    for (int i = 0; str[i] != '\0'; i++)
    {
        if (str[i] == charToFind)
            ++frequency;
    }
    printf("Found %c %d times in %s\n", charToFind, frequency, str);
}

void exer6(void)
{
    int intArr[] = {3, 6, 2, 8, 5, 9, 1, 7, 1};
    int len = (sizeof(intArr) / sizeof(int));
    float aver = 0;
    calcAver(intArr, len, &aver);
    printf("Average %.2f", aver);
}

void calcAver(int *arr, int len, float *average)
{
    float sum = 0;
    for (int i = 0; i < len; i++)
    {
        sum += arr[i];
    }
    *average = sum / len;
}

void exer7(void)
{
    float fArr[] = {5.67f, 6.78f, 9.34f};
    int len = sizeof(fArr) / sizeof(float);
    // using index
    for (int i = 0; i < len; i++)
    {
        printf("Elem: %f at %p\n", fArr[i], &fArr[i]);
    }
    // using pointer notation
    for (int i = 0; i < len; i++)
    {
        printf("Elem: %.2f at %p\n", *(fArr + i), &fArr[i]);
    }

    float *fPtr, sum;
    int size = 3, i = 0;
    fPtr = (float *)malloc(size * sizeof(float));

    if (fPtr == NULL)
    {
        printf("Error! memory not allocated.");
        exit(0);
    }

    printf("Enter %d elements:\n", size);
    for (i = 0; i < size; ++i)
    {
        scanf("%f", fPtr + i);
        sum += *(fPtr + i);
    }
    for (int i = 0; i < size + 1; i++)
    {
        printf("Elem: %.2f at %p\n", *(fPtr + i), &fPtr[i]);
    }

    printf("Sum = %.2f\n", sum);
    fPtr = (float *)realloc(fPtr, (size + 1) * sizeof(float));
    *(fPtr + 3) = 3.56;
    for (int i = 0; i < size + 1; i++)
    {
        printf("Elem: %.2f at %p\n", *(fPtr + i), &fPtr[i]);
    }
    free(fPtr);
}