#include <stdio.h>

void incr(int *i);
void incre(int i);

int main() {
    int num = 0;
    int *ptr = &num;
    printf("Number: %d\n", num);
    incre(num);
    printf("Number: %d\n", num);
    return 0;
}

void incre(int i) {
    ++i;
    printf("Number: %d\n", i);
}

void incr(int *i) {
    ++*i;
    printf("Number: %d\n", *i);
}