#include<pthread.h>
#include<stdio.h>

void *menu(void *ptr)
{
    int id = (int) ptr;
    int selection;
    int count = 0;

    for( ; ;) {

        printf("Select your level: \n");
        printf("1.\t Beginner\n");
        printf("2.\t Intermediate\n");
        printf("3.\t Advanced\n");
        printf("4.\t Exit\n");
        
        printf("\nEnter your level: ");
        scanf("%d", &selection);
        if(selection < 0 || selection > 4) {
            printf("%d\n", selection);
            printf("Enter a correct level!\n");
            continue;
        }
        
        switch(selection) {
            case 1:
                printf("\nBeginner\n");
                break;
            case 2:
                printf("\nIntermediate\n");
                break;
            case 3:
                printf("\nAdvanced\n");
                break;
            case 4:
                printf("\nByebye\n");
                return 0;
                break;
            default:
                printf("Enter a correct level!\n");
                break;
        }
    }
    return ptr;
}

int main(int argc, char **argv)
{
    pthread_t thread1, thread2;
    int thr = 1;
    menu((void*)thr);
    //pthread_create(&thread1, NULL, *menu, (void *) thr);
    //printf("Do something else\n");
    //pthread_exit(NULL);
    return 0;
}