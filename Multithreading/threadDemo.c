#include<pthread.h>
#include<stdio.h>

int g = 0; 

void *threadFunc(void *ptr)
{
    int id = (int) ptr;
    ++g; // change global
    fprintf(stderr,"Thread id %d, global %d\n", id, g);
    return  ptr;
}

int main(int argc, char **argv)
{
    pthread_t thread1, thread2;
    int thr = 1;
    int thr2 = 2;
    pthread_create(&thread1, NULL, *threadFunc, (void *) thr);
    pthread_create(&thread2, NULL, *threadFunc, (void *) thr2);
    pthread_exit(NULL);
    return 0;
}