#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

pthread_mutex_t lock;
int j;

void *doProcess(void *ptr)
{
    pthread_mutex_lock(&lock);
    int id = (int) ptr;
    int i = 0;
    printf("ThreadId %d\n", id);
    j++;
    while (i < 5)
    {
        printf("%d", j);
        sleep(1);
        i++;
    }
    printf("...Done\n");
    pthread_mutex_unlock(&lock);
    return ptr;
}

int main(void)
{
    int err;
    pthread_t t1, t2;
    int thrId1 = 1, thrId2 = 2;
    if (pthread_mutex_init(&lock, NULL) != 0)
    {
        printf("Mutex initialization failed.\n");
        return 1;
    }

    j = 0;
    pthread_create(&t1, NULL, doProcess, (void *) thrId1);
    pthread_create(&t2, NULL, doProcess, (void *) thrId2);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    return 0;
}