#include "pruef3.h"

char name[100];

int main()
{
    //exer1();
    //exer1b();
    //exer2();
    /*
    float *arr = exer3();
    for(int i = 0; i < 10; i++) {
        printf("r[%d] = %f\n", i, arr[i]);
    }
    int year = 2025;
    printf("Year %d Chinese Zodiac: %s\n", year, exer4(year));

   unsigned int num = 4;
   printf("Factorial of %d: %d\n", num, exer5(num));
   printf("Factorial of %d: %d\n", num, exer5b(num));
   */
   exer6();
   exer7();
   printf("Name %s is palindrom %s", name, exer8() ? "true" : "false");
}

void exer1(void)
{
    char str[] = "Wer reitet so spät durch Nacht und Wind";
    int i = 0;
    while(str[i] != '\0') {
        if(str[i] == ' ') {
            printf("\n");
            ++i;
        }
        printf("%c",str[i]);
        ++i;
    }
    printf("\n");
    // oder mit tokenizer
    char *token = strtok(str, " ");
    while (token)
    {
        printf("Token: %s\n", token);
        token = strtok(NULL, " ");
    }
}

void exer1b() {
    char str[] = "Wer reitet so spät durch Nacht und Wind";
    char str2[strlen(str)];
    int i = 0, c = 0;
    while(str[i] != '\0') {
        if(str[i] != 'e') {
            str2[c] = str[i];
            ++c;
        }
        ++i;
    }
    puts(str2);
}

void exer2(void) {
    char str[] = "The stering where the word the present more than once";
    char sstr[] = "the";
    int len = strlen(str), freq = 0;
    for(int i = 0; i < len-3; i++) {
        if((str[i] == 't' || str[i] == 'T') &&
           (str[i+1] == 'h' || str[i+1] == 'H') &&
           (str[i+2] == 'e' || str[i+2] == 'E')) {
            ++freq;
        }
           
    }
    printf("%s occures %d times\n", sstr, freq);
}

float* exer3(void) {
    static float  r[10];
    int i;
   srand( (unsigned)time(NULL));
   for ( i = 0; i < 10; ++i) {
      r[i] = ((float)rand()/(float)(RAND_MAX)) * 10.0;
      //printf( "r[%d] = %f\n", i, r[i]);
   }
   return r;
}

char* exer4(int year) {
    const char* chineseZodiac[] = {"Monkey", "Rooster", "Dog", "Pig", "Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Goat"};
    return chineseZodiac[year%12];
}

unsigned long long int exer5(unsigned int num) {
    int fac = 1;
    for(int i = num; i > 1; i--) {
        fac *= i;
    }
    return fac;
}

unsigned long long int exer5b(unsigned int num) {
    if(num <= 1) {
        return 1;
    }
    return num * exer5b(num -1 );
}

void exer6() {
    int intArr[] = {3, 6, 2, 38, 5, 9, 1, -11, 2, 5};
    int smallest = 0, biggest = 0, len = sizeof(intArr)/sizeof(int);
    for(int i = 0; i < len; i++) {
        if(intArr[i] > biggest)
            biggest = intArr[i];
    }
    smallest = biggest;
    for(int i = 0; i < len; i++) {
        if(intArr[i] < smallest)
            smallest = intArr[i];
    }

    printf("Biggest %d smallest %d\n", biggest, smallest);
}

void exer7(void) {
    int intArr[] = {3, 6, 2, 38, 5, 9, 1, 11, 2, 5};
    int i = 0, evenCount = 0, oddCount = 0, len = sizeof(intArr)/sizeof(int);
    int evenArr[len], oddArr[len];
    for(i = 0; i < len; i++) {
        if(intArr[i]%2 == 0) {
            evenArr[evenCount] = intArr[i];
            evenCount++;
        } else {
            oddArr[oddCount] = intArr[i];
            oddCount++;
        }
    }
    for(i = 0; i < evenCount; i++) {
        printf("%d ", evenArr[i]);
    }
    printf("\n");
    for(i = 0; i < oddCount; i++) {
        printf("%d ", oddArr[i]);
    }
}

bool exer8() {
    printf("Enter name:\n");
    scanf("%s", &name);
    printf("Name %s\n", name);
    strupr(name);
    int len = strlen(name) - 1;
    int last = len;
    bool isPalin = true;
    for(int i = 0; i < len; i++) {
        if(name[i] != name[last]) {
            return false;
        }
        last--;
    }
    return isPalin;
}