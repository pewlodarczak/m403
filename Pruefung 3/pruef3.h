#ifndef PRUEF3_H
#define PRUEF3_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

void exer1(void);
void exer1b(void);
void exer2(void);
float* exer3(void);
char* exer4(int year);
unsigned long long int exer5(unsigned int num);
unsigned long long int exer5b(unsigned int num);
void exer6(void);
void exer7(void);
bool exer8(void);

#endif